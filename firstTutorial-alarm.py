import sys, time
from PySide2 import QtCore, QtWidgets, QtGui

app = QtWidgets.QApplication(sys.argv)

try:
    due = QtCore.QTime.currentTime()
    message ="Alert"
    
    if len(sys.argv) < 2:
        raise ValueError
    
    hours,minutes = sys.argv[1].split(":")
    due = QtCore.QTime(int(hours),int(minutes))
    
    if not due.isValid():
        raise ValueError
        
    if len(sys.argv) > 2:
        message = " ".join(sys.argv[2:])
    
except ValueError:
    message = "Usage: firstTutorial.py HH:MM [jptional message]" #24 hour clock
    
while QtCore.QTime.currentTime() < due:
    time.sleep(10)
    
    
    
label = QtWidgets.QLabel("<font color=red size=72><b>" + message + "</b></font>")
label.setWindowFlags(QtCore.Qt.SplashScreen)
label.show()

QtCore.QTimer.singleShot(20000, app.quit) #20 seconds
app.exec_()
