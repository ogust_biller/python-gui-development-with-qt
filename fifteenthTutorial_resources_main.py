# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'fifteenthTutorial-resources-main.ui'
##
## Created by: Qt User Interface Compiler version 5.14.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QMetaObject, QObject, QPoint,
    QRect, QSize, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QLinearGradient, QPalette, QPainter, QPixmap,
    QRadialGradient)
from PySide2.QtWidgets import *

import icons_rc

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(541, 408)
        icon = QIcon()
        icon.addFile(u":/icons/icons_qt_extended_64x64.png", QSize(), QIcon.Normal, QIcon.Off)
        Dialog.setWindowIcon(icon)
        self.fbButton = QPushButton(Dialog)
        self.fbButton.setObjectName(u"fbButton")
        self.fbButton.setGeometry(QRect(10, 50, 88, 88))
        self.fbButton.setLayoutDirection(Qt.LeftToRight)
        icon1 = QIcon()
        icon1.addFile(u":/icons/facebook.png", QSize(), QIcon.Normal, QIcon.Off)
        self.fbButton.setIcon(icon1)
        self.fbButton.setIconSize(QSize(48, 48))
        self.fbButton.setFlat(False)
        self.instaButton = QPushButton(Dialog)
        self.instaButton.setObjectName(u"instaButton")
        self.instaButton.setGeometry(QRect(120, 50, 88, 88))
        self.instaButton.setLayoutDirection(Qt.LeftToRight)
        icon2 = QIcon()
        icon2.addFile(u":/icons/instagram-new.png", QSize(), QIcon.Normal, QIcon.Off)
        self.instaButton.setIcon(icon2)
        self.instaButton.setIconSize(QSize(48, 48))
        self.instaButton.setFlat(False)
        self.twitterButton = QPushButton(Dialog)
        self.twitterButton.setObjectName(u"twitterButton")
        self.twitterButton.setGeometry(QRect(230, 50, 88, 88))
        self.twitterButton.setLayoutDirection(Qt.LeftToRight)
        icon3 = QIcon()
        icon3.addFile(u":/icons/twitter.png", QSize(), QIcon.Normal, QIcon.Off)
        self.twitterButton.setIcon(icon3)
        self.twitterButton.setIconSize(QSize(48, 48))
        self.twitterButton.setFlat(False)
        self.vkButton = QPushButton(Dialog)
        self.vkButton.setObjectName(u"vkButton")
        self.vkButton.setGeometry(QRect(340, 50, 88, 88))
        self.vkButton.setLayoutDirection(Qt.LeftToRight)
        icon4 = QIcon()
        icon4.addFile(u":/icons/vk-com.png", QSize(), QIcon.Normal, QIcon.Off)
        self.vkButton.setIcon(icon4)
        self.vkButton.setIconSize(QSize(48, 48))
        self.vkButton.setFlat(False)
        self.okButton = QPushButton(Dialog)
        self.okButton.setObjectName(u"okButton")
        self.okButton.setGeometry(QRect(440, 50, 88, 88))
        self.okButton.setLayoutDirection(Qt.LeftToRight)
        icon5 = QIcon()
        icon5.addFile(u":/icons/odnoklassniki.png", QSize(), QIcon.Normal, QIcon.Off)
        self.okButton.setIcon(icon5)
        self.okButton.setIconSize(QSize(48, 48))
        self.okButton.setFlat(False)

        self.retranslateUi(Dialog)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Share to", None))
        self.fbButton.setText(QCoreApplication.translate("Dialog", u"PushButton", None))
        self.instaButton.setText(QCoreApplication.translate("Dialog", u"PushButton", None))
        self.twitterButton.setText(QCoreApplication.translate("Dialog", u"PushButton", None))
        self.vkButton.setText(QCoreApplication.translate("Dialog", u"PushButton", None))
        self.okButton.setText(QCoreApplication.translate("Dialog", u"PushButton", None))
    # retranslateUi

