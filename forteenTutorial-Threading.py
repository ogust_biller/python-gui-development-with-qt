# This Python file uses the following encoding: utf-8

import sys
import time

from PySide2.QtCore import QThread, Signal
from PySide2.QtCore import Qt
from PySide2.QtWidgets import QApplication, QDialog, QMessageBox
from showGui import Ui_mainDialog

__appname__ = 'Implementation ui in python code'


class MainDialog(QDialog, Ui_mainDialog):

    def __init__(self, parent=None):
        super(MainDialog, self).__init__(parent)
        self.setupUi(self)

        self.workerThread = WorkerThread()

        self.showButton.setText('Process')
        self.showButton.clicked.connect(self.processData)
        self.workerThread.atDone.connect(self.threadDone, Qt.DirectConnection)

    def threadDone(self):
        QMessageBox.warning(self, 'Done', 'Thread execution complte')

    def processData(self):
        self.workerThread.start()
        QMessageBox.information(self, "Done!", "Done.")


class WorkerThread(QThread):
    def __init__(self, parent=None):
        super(WorkerThread, self).__init__(parent)

    atDone = Signal()

    def run(self):
        time.sleep(5)
        self.atDone.emit()


app = QApplication(sys.argv)
form = MainDialog()
form.show()
app.exec_()
