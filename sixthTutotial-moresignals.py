# This Python file uses the following encoding: utf-8
import sys
from PySide2.QtCore import Signal
from PySide2.QtWidgets import QApplication, QDialog, QSpinBox,\
                              QDial, QHBoxLayout


class ZeroSpinBox(QSpinBox):

    zeros = 0

    atzero = Signal(int)

    def __init__(self, parent=None):

        super(ZeroSpinBox, self).__init__(parent)

        self.valueChanged.connect(self.checkzero)

    def checkzero(self):
        if self.value() == 0:
            print("zero")
            self.zeros += 1
            self.atzero.emit(self.zeros)


class Form(QDialog):

    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        dial = QDial()
        dial.setNotchesVisible(True)

        zerospinbox = ZeroSpinBox()

        layout = QHBoxLayout()
        layout.addWidget(dial)
        layout.addWidget(zerospinbox)
        self.setLayout(layout)

        dial.valueChanged.connect(zerospinbox.setValue)
        zerospinbox.valueChanged.connect(dial.setValue)
        zerospinbox.atzero.connect(self.announce)

        self.setWindowTitle('Siganls and Slots')

    def announce(self, zeros):
        print('ZeroSpinBox haz been at zeros '+str(zeros)+' times')


app = QApplication(sys.argv)
form = Form()
form.show()
app.exec_()
