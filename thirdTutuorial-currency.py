import sys
import re
from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from urllib import request
import urllib

class Form(QDialog):
    def __init__(self, parent=None):
        super(Form, self).__init__(parent)
        
        date = self.getdate()
        #rates = sorted(self.rates.keys())
        rates = self.rates
        self.amount = 1.00
        
        datelabel = QLabel(date)
        self.fromComboBox = QComboBox()
        self.fromComboBox.addItems([r.get('cur_name') for r in self.rates])
        self.fromSpinBox = QDoubleSpinBox()
        self.fromSpinBox.setRange(0.01,100000000.00)
        self.fromSpinBox.value = 1.00
        self.toComboBox = QComboBox()
        self.toComboBox.addItems([r.get('cur_name') for r in self.rates])
        self.toLabel = QLabel('1.00')
        
        grid = QGridLayout()                          #      cols:  0       1      2
        grid.addWidget(datelabel,0,0)                 #rows     0
        grid.addWidget(self.fromComboBox,1,0)         #         1
        grid.addWidget(self.fromSpinBox,1,1)          #         2 
        grid.addWidget(self.toComboBox,2,0)
        grid.addWidget(self.toLabel,2,1)
        self.setLayout(grid)
        
        self.connect(self.fromComboBox, SIGNAL("currentIndexChanged(int)"), self.updateUi)
        self.connect(self.toComboBox, SIGNAL("currentIndexChanged(int)"), self.updateUi)
        #self.connect(self.fromSpinBox, SIGNAL("valueChanged(double)"), self.updateUi)
        self.connect(self.fromSpinBox, SIGNAL("valueChanged(double)"), self.valueChanged)


    def valueChanged(self,d):
        self.amount = d
        self.updateUi()


        
    def getratebyname(self, name):
        for i in self.rates:
            if name == i['cur_name']:
                return i['rate'] / i['amount'] 
        
        
    def updateUi(self):
        to = self.toComboBox.currentText()
        from_ = self.fromComboBox.currentText()
        amountrate = (self.getratebyname(from_)/self.getratebyname(to))*self.amount
        self.toLabel.setText("%0.2f" % amountrate)
        
        
    def getdate(self):
        self.rates = []
        
        try:
            date = 'Unknown'
            
            fh = urllib.request.urlopen("https://www.cbr.ru/currency_base/daily/").read().decode('utf-8', errors='ignore')
            fh = fh.replace('\n','')
            rows = re.findall(string=fh, pattern='<tr>(.+?)</tr>')
            rows = rows[1:]
            for i in range(len(rows)):
                vals= (rows[i].replace('\r','')).replace('  ','').replace('<td>','').split('</td>')
                self.rates.append(dict(cur_id=vals[0], cur_code=vals[1], amount=int(vals[2]), cur_name=vals[3], rate=float(vals[4].replace(',','.'))))
            self.rates.append(dict(cur_id='RUR', cur_code='RUR', amount=1, cur_name='Росийский рубль',rate=1.00))
            self.rates=self.rates[1:]
            k=[r.get('cur_name') for r in self.rates]
            
            return 'date'
        except:
            return "Failure to process: \n"

app = QApplication(sys.argv)
form = Form()
form.show()
app.exec_()
