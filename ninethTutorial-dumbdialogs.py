# This Python file uses the following encoding: utf-8
import sys
# from PySide2.QtCore import Signal
from PySide2.QtWidgets import QApplication, QDialog, QPushButton, QLabel,\
                                 QVBoxLayout, QGridLayout, QCheckBox, QSpinBox, \
                                 QMessageBox


__apname__ = 'Ninth Tutorial'

class Maiform(QDialog):

    def __init__(self, parent=None):
        super(Maiform, self).__init__(parent)

        self.setWindowTitle(__apname__)

        btn = QPushButton('Open dialog')
        self.label1 = QLabel('Label 1 result')
        self.label2 = QLabel('Label 2 result')

        layout = QVBoxLayout()
        layout.addWidget(btn)
        layout.addWidget(self.label1)
        layout.addWidget(self.label2)
        self.setLayout(layout)

        btn.clicked.connect(self.dialogOpen)

    def dialogOpen(self):
        dlg = MyDialog()
        if dlg.exec_():
            self.label1.setText('Spinbox value is: '+str(dlg.spinbox.value()))
            self.label2.setText('Checbox is: '+str(dlg.checkbox.isChecked()))
        else:
            self.label1.setText('<unknown>')
            self.label2.setText('<unknown>')
            QMessageBox.warning(self, '', 'Dialog cancelled')


class MyDialog(QDialog):

        def __init__(self, parent=None):
            super(MyDialog, self).__init__(parent)

            self.setWindowTitle('MyDialog')

            self.checkbox = QCheckBox('Option 1')
            self.spinbox = QSpinBox()
            buttonOk = QPushButton('OK')
            buttonCancel = QPushButton('Cancel')

            layout = QGridLayout()
            layout.addWidget(self.spinbox,0,0)
            layout.addWidget(self.checkbox,0,1)
            layout.addWidget(buttonCancel)
            layout.addWidget(buttonOk)
            self.setLayout(layout)

            buttonOk.clicked.connect(self.accept)
            buttonCancel.clicked.connect(self.reject)







app = QApplication(sys.argv)
form = Maiform()
form.show()
app.exec_()




# if__name__ == "__main__":
#     pass
