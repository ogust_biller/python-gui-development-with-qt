# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ui_mainForm.ui'
##
## Created by: Qt User Interface Compiler version 5.14.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QMetaObject, QObject, QPoint,
    QRect, QSize, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QLinearGradient, QPalette, QPainter, QPixmap,
    QRadialGradient)
from PySide2.QtWidgets import *


class Ui_mainDialog(object):
    def setupUi(self, Ui_mainDialog):
        if Ui_mainDialog.objectName():
            Ui_mainDialog.setObjectName(u"Ui_mainDialog")
        Ui_mainDialog.resize(339, 63)
        self.nameEdit = QLineEdit(Ui_mainDialog)
        self.nameEdit.setObjectName(u"nameEdit")
        self.nameEdit.setGeometry(QRect(10, 20, 201, 20))
        self.showButton = QPushButton(Ui_mainDialog)
        self.showButton.setObjectName(u"showButton")
        self.showButton.setGeometry(QRect(220, 20, 75, 23))

        self.retranslateUi(Ui_mainDialog)

        QMetaObject.connectSlotsByName(Ui_mainDialog)
    # setupUi

    def retranslateUi(self, Ui_mainDialog):
        Ui_mainDialog.setWindowTitle(QCoreApplication.translate("Ui_mainDialog", u"Dialog", None))
        self.showButton.setText(QCoreApplication.translate("Ui_mainDialog", u"Show", None))
    # retranslateUi

