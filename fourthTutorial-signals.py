# This Python file uses the following encoding: utf-8
import sys

from PySide2.QtCore import SIGNAL
from PySide2.QtWidgets import QApplication, QDialog, QSpinBox,     \
                              QDial, QHBoxLayout


class Form(QDialog):

    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        dial = QDial()
        dial.setNotchesVisible(True)

        spinbox = QSpinBox()

        layout = QHBoxLayout()
        layout.addWidget(dial)
        layout.addWidget(spinbox)
        self.setLayout(layout)

        self.connect(dial, SIGNAL("valueChanged(int)"), spinbox.setValue)
        self.connect(spinbox, SIGNAL("valueChanged(int)"), dial.setValue)

        self.setWindowTitle('Siganls and Slots')


app = QApplication(sys.argv)
form = Form()
form.show()
app.exec_()
