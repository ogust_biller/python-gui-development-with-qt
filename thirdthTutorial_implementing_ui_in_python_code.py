# This Python file uses the following encoding: utf-8

import sys

from PySide2.QtCore import (QCoreApplication, QMetaObject, QObject, QPoint,
    QRect, QSize, QUrl, Qt, Signal)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QLinearGradient, QPalette, QPainter, QPixmap,
    QRadialGradient)
from PySide2.QtWidgets import *
from showGui import Ui_mainDialog

__appname__='Implementation ui in python code'


class MainDialog(QDialog, Ui_mainDialog):

    def __init__(self, parent=None):
        super(MainDialog,self).__init__(parent)
        self.setupUi(self)

        self.ShowButton.clicked.connect(self.showMessageBox)

    def showMessageBox(self):
        QMessageBox.information(self, 'title', self.nameEdit.text())



app = QApplication(sys.argv)
form = MainDialog()
form.show()
app.exec_()
