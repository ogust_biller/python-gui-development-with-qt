# This Python file uses the following encoding: utf-8
import sys, os
from PySide2.QtCore import Signal
from PySide2.QtWidgets import QApplication, QDialog, QPushButton, \
                                 QVBoxLayout, QFileDialog

__appname__ = 'Eight Tutorial'

class Mainform(QDialog):

    def __init__(self, parent=None):
        super(Mainform, self).__init__(parent)
        openButton = QPushButton('Open...')
        saveButton = QPushButton('Save...')
        dirButton = QPushButton('Directory open')
        closeButton = QPushButton('Close')
        layout = QVBoxLayout()
        layout.addWidget(openButton)
        layout.addWidget(saveButton)
        layout.addWidget(dirButton)
        layout.addWidget(closeButton)
        self.setLayout(layout)

        openButton.clicked.connect(self.open)
        saveButton.clicked.connect(self.save)
        dirButton.clicked.connect(self.changedir)
        closeButton.clicked.connect(self.close)

    def save(self):

        dir = '.'
        contents = "print('Hello, world!')"

        fileObj = QFileDialog.getSaveFileName(parent=self, caption='Open file', dir=dir, filter='Python code (*.py)')
        open(fileObj[0],'w').write(contents)

    def open(self):

         dir = '.'

         fileObj = QFileDialog.getOpenFileName(parent=self, caption='Open file', dir=dir, filter='Python code (*.py)')
         file = open(fileObj[0],'r')
         file.close()

    def changedir(self):

        dir = '.'

        dir = QFileDialog.getExistingDirectory(self, caption='Change default directory', dir=dir)
        os.chdir(dir)




app = QApplication(sys.argv)
form = Mainform()
form.show()
app.exec_()
# if__name__ == "__main__":
#     pass
