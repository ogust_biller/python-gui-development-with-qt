import sys
from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
from math import *


class Form(QDialog):
    def __init__(self, parent=None):
        super(Form, self).__init__(parent)
        
        self.browser = QTextBrowser()
        self.lineedit = QLineEdit()
        self.lineedit.setPlaceholderText("Type an expression an press Enter")
        self.lineedit.selectAll()
        
        layout = QVBoxLayout()
        layout.addWidget(self.browser)
        layout.addWidget(self.lineedit)
        self.setLayout(layout)
        
        self.lineedit.setFocus()
        
        self.connect(self.lineedit, SIGNAL("returnPressed()"), self.updateUi)
        self.setWindowTitle("Calculator")
        
        
    def updateUi(self):
        try:
            text = self.lineedit.text()
            self.browser.append("%s = <b>%s</b>" % (text, eval(text)))
            self.lineedit.selectAll()
        except:
            self.browser.append("<font color=red>%s is invalid</font>" % text)
            
            
app = QApplication(sys.argv)
form = Form()
form.show()
app.exec_()
