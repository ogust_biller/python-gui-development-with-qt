# This Python file uses the following encoding: utf-8
import sys
# from PySide2.QtCore import Signal
from PySide2.QtWidgets import QApplication, QDialog, QPushButton, QLabel,\
                                 QVBoxLayout, QGridLayout, QCheckBox, QSpinBox, \
                                 QMessageBox


__apname__ = 'Tenth Tutorial'

class Maiform(QDialog):

    def __init__(self, parent=None):
        super(Maiform, self).__init__(parent)

        self.setWindowTitle(__apname__)

        btn = QPushButton('Open dialog')
        self.mainSpinBox = QSpinBox()
        self.mainCheckBox= QCheckBox()

        layout = QVBoxLayout()
        layout.addWidget(self.mainSpinBox)
        layout.addWidget(self.mainCheckBox)
        layout.addWidget(btn)
        self.setLayout(layout)

        btn.clicked.connect(self.dialogOpen)

    def dialogOpen(self):
        initvalues = {'mainSpinBox':self.mainSpinBox.value(),\
                      'mainCheckBox':self.mainCheckBox.isChecked()}
        dlg = MyDialog(initvalues)
        if dlg.exec_():
            self.mainSpinBox.setValue(dlg.spinbox.value())
            self.mainCheckBox.setChecked(dlg.checkbox.isChecked())


class MyDialog(QDialog):

    def __init__(self, initValues, parent=None):
            super(MyDialog, self).__init__(parent)

            self.setWindowTitle('MyDialog')

            self.checkbox = QCheckBox('Option 1')
            self.spinbox = QSpinBox()
            buttonOk = QPushButton('OK')
            buttonCancel = QPushButton('Cancel')

            layout = QGridLayout()
            layout.addWidget(self.spinbox,0,0)
            layout.addWidget(self.checkbox,0,1)
            layout.addWidget(buttonCancel)
            layout.addWidget(buttonOk)
            self.setLayout(layout)

            self.spinbox.setValue(initValues['mainSpinBox'])
            self.checkbox.setChecked(initValues['mainCheckBox'])

            buttonOk.clicked.connect(self.accept)
            buttonCancel.clicked.connect(self.reject)

    def accept(self):

        class GreaterThanFive(Exception): pass
        class IsZero(Exception): pass

        try:
            if self.spinbox.value() >5:
                raise GreaterThanFive("The spinbox value cannot greater then 5")
            elif self.spinbox == 0:
                raise IsZero("The spinbox velue can not equal 0")
            else:
                QDialog.accept(self)

        except GreaterThanFive as e:
            QMessageBox.warning(self, __apname__, str(e))
            self.spinbox.selectAll()
            self.spinbox.setFocus()
            return

        except IsZero as e:
            QMessageBox.warning(self, __apname__, str(e))
            self.spinbox.selectAll()
            self.spinbox.setFocus()
            return








app = QApplication(sys.argv)
form = Maiform()
form.show()
app.exec_()




# if__name__ == "__main__":
#     pass
