import sys
from PySide2.QtWidgets import QApplication, QDialog
from fifteenthTutorial_resources_main import Ui_Dialog

__appname__ = 'Implementation ui in python code'


class MainDialog(QDialog, Ui_Dialog):

    def __init__(self, parent=None):
        super(MainDialog, self).__init__(parent)
        self.setupUi(self)


app = QApplication(sys.argv)
form = MainDialog()
form.show()
app.exec_()
